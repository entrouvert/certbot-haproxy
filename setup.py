import os
import subprocess
import sys
from setuptools import setup
from setuptools import find_packages
from distutils.command.sdist import sdist

own_version = '0.1.2'
certbot_version = '0.8.1'

class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git', 'describe', '--dirty', '--match=v*'], stdout=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            version = result.split()[0][1:]
            version = version.replace('-', '.')
            return version
    return '0'

def data_tree(destdir, sourcedir):
    extensions = ['.css', '.png', '.jpeg', '.jpg', '.gif', '.xml', '.html', '.js']
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in extensions]
        r.append((root.replace(sourcedir, destdir, 1), l))
    return r

# Please update tox.ini when modifying dependency version requirements
install_requires = [
    'acme>={0}'.format(certbot_version),
    'certbot>={0}'.format(certbot_version),
    # For pkg_resources. >=1.0 so pip resolves it to a version cryptography
    # will tolerate; see #2599:
    'setuptools>=1.0',
    'zope.component',
    'zope.interface',
    'future',
]

if sys.version_info < (2, 7):
    install_requires.append('mock<1.1.0')
else:
    install_requires.append('mock')

docs_extras = [
    'Sphinx>=1.0',  # autodoc_member_order = 'bysource', autodoc_default_flags
    'sphinx_rtd_theme',
]

long_description = (
    "This is a plugin for Certbot, it enables automatically authenticating "
    "domains ans retrieving certificates. It can also restart HAProxy after "
    "new certificates are installed. However, it will not configure HAProxy "
    "because. HAProxy is unlikely to be used for small/simple setups like what"
    " Apache or NGiNX are more likely to be used for. HAProxy configurations "
    "vary greatly, any configuration this plugin could define is most likely "
    "not applicable in your environment."
)

haproxy_authenticator = 'certbot_haproxy.authenticator:HAProxyAuthenticator'
haproxy_installer = 'certbot_haproxy.installer:HAProxyInstaller'

setup(
    name='certbot-haproxy',
    version=get_version(),
    description="HAProxy plugin for Certbot",
    long_description=long_description,
    url='https://code.greenhost.net/open/certbot-haproxy',
    author="Greenhost BV",
    author_email='lehaproxy@greenhost.net',
    license='Apache License 2.0',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Plugins',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Security',
        'Topic :: System :: Installation/Setup',
        'Topic :: System :: Networking',
        'Topic :: System :: Systems Administration',
        'Topic :: Utilities',
    ],

    packages=find_packages(),
    include_package_data=True,
    data_files=data_tree('share/certbot-haproxy/docs/', 'docs/'),
    install_requires=install_requires,
    extras_require={
        'docs': docs_extras,
    },
    entry_points={
        'certbot.plugins': [
            'haproxy-authenticator = %s' % haproxy_authenticator,
            'haproxy-installer = %s' % haproxy_installer
        ],
    },
    # test_suite='certbot_haproxy',
    cmdclass={
        'sdist': eo_sdist,
    }
)
